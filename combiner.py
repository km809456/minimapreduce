'''
taks1 shuffle
group by airportName(key), the record(value) into a list
{(name1:[1,1,1,1,1]),(name2:[1,1,1,1,1,1])}
'''
def ap_shuffle(mapper_out):
    data = {}
    mapper_out = list(filter(None,mapper_out))
    for k,v in mapper_out:
        if k not in data:
            data[k] = [v]
        else:
            data[k].append(v)
    return data

'''
taks2 shuffle
inputdata list : [(passengerId1_flightId1(key),1),passengerId1_flightId1(key),1),passengerId1_flightId2(key),1)]
group by passengerId(key), the record(value) into a list
outputdata:
{(passengerId1:[1,1,1,1,1]),(passengerId2:[1,1,1,1,1,1])}
'''
def pa_shuffle(mapper_out):
    data = {}
    #use set to remove the duplicate data
    mapper_out = set(filter(None, mapper_out))

    for k, v in mapper_out:
        passengerId = k.split('_')[0]
        if passengerId not in data:
            data[passengerId] = [v]
        else:
            data[passengerId].append(v)

    return data




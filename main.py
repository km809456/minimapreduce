
'''
task1: just need change the import name;
task2: just change the import name from ap_xxxx to pa_xxxx e.g.
        'from combiner import pa_shuffle as shuffle'
        'from mapper import pa_map as map_func'
'''
from combiner import pa_shuffle as shuffle
from mapper import pa_map as map_func
from reducer import reduce as reduce_func

#import multi lib
import multiprocessing as mp
# main function start from here
if __name__ == '__main__':
    # open the datafile
    with open('data/AComp_Passenger_data_no_error.csv',encoding="utf-8") as f:
        # read the file line by line save into map_in
        map_in = f.read().splitlines()
        #init threadsPool by cpu count
    with mp.Pool(processes=mp.cpu_count()) as pool:
        #map function
        map_out = pool.map(map_func,map_in,chunksize=int(len(map_in)/mp.cpu_count()))
        #shuffle fucntion
        reduce_in = shuffle(map_out)
        #reduce function
        reduce_out = pool.map(reduce_func,reduce_in.items(),chunksize=int(len(reduce_in.keys())/mp.cpu_count()))
        #the result need to be sorted by desc
        reduce_out.sort(key=lambda pair:pair[1],reverse=True)
        print(reduce_out)


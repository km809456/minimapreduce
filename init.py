#init the Airport name and code into a dict
'''
name    code    fearture3  feature4
ATLANTA,ATL,33.636719,-84.428067
'''
def _init():
    data = {}
    with open('data/Top30_airports_LatLong.csv', encoding="utf-8") as f:
        airport_info = f.read().splitlines()
        airport_info = list(filter(None, airport_info))
        for line in airport_info:
                cols = line.split(',')
                code = cols[1]
                name = cols[0]
                if code not in data:
                    data[code] = name

        return data
